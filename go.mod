module gitlab.com/kiarsy/go-v2ray-tun2socks-ios

go 1.13

require (
	github.com/eycorsican/go-tun2socks v1.16.8
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/v2ray/v2ray-core v4.15.0+incompatible
	gitlab.com/kiarsy/tun2socks v0.0.0-20210123092955-37ba1b562a85
	golang.org/x/mobile v0.0.0-20201217150744-e6ae53a27f4f // indirect
	v2ray.com/core v4.19.1+incompatible
)
